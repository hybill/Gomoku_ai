//
//  node.swift
//  Gomoku
//
//  Created by YWQ on 11/6/16.
//  Copyright © 2016 ywq. All rights reserved.
//

import Foundation
class node:NSObject{
    var value = Int()
    var x = Int()
    var y = Int()
    init(x:Int,y:Int,value:Int) {
        self.value = value
        self.x = x
        self.y = y
    }
}
