//
//  Agent.swift
//  Gomoku
//
//  Created by YWQ on 10/29/16.
//  Copyright © 2016 ywq. All rights reserved.
//

import Foundation
import UIKit
class Agent{
    let LIMIT_DEPTH = 1

    //func startAI(map:[[Int]],isblack:Bool)->CGPoint{
        
//grade the space
    func check4direction(board:[[Int]],X:Int,Y:Int,isblack:Bool,direction:String)->Int{
        var model:String = "A"
        var X1 = 0
        var X2 = 0
        var Y1 = 0
        var Y2 = 0
        
            switch direction {
            case "DtoU":
                X1 = X
                Y1 = Y-1
                var cout = 0
                while(Y1 >= 0 && cout<5){
                    model = model + checkbit(chess: board[Y1][X1],isblack:isblack)
                    Y1 -= 1
                    cout += 1
                }
                X2 = X
                Y2 = Y+1
                cout = 0
                while(Y2 < size && cout<5){
                    model = checkbit(chess: board[Y2][X2],isblack:isblack) + model
                    Y2 += 1
                    cout += 1
                }
                break
            case "LtoR":
                X1 = X+1
                Y1 = Y
                var cout = 0
                while(X1 < size && cout<5){
                    model = model + checkbit(chess: board[Y1][X1],isblack:isblack)
                    X1 += 1
                    cout += 1
                }
               X2 = X-1
               Y2 = Y
                cout = 0
                while(Y2 >= 0 && X2 >= 0 && cout<5){
                    model = checkbit(chess: board[Y2][X2],isblack:isblack) + model
                    X2 -= 1
                    cout += 1
                }
                break
            case "rightup":
                X1 = X+1
                Y1 = Y-1
                var cout = 0
                while(X1 < size && Y1 >= 0 && cout<5){
                    model = model + checkbit(chess: board[Y1][X1],isblack:isblack)
                    X1 += 1
                    Y1 -= 1
                    cout += 1
                }
                X2 = X-1
                Y2 = Y+1
                cout = 0
                while(X2  >= 0 && Y2 < size && cout<5){
                    model = checkbit(chess: board[Y2][X2],isblack:isblack) + model
                    X2 -= 1
                    Y2 += 1
                    cout += 1
                }
                break
            case "leftup":
                X1 = X+1
                Y1 = Y+1
                var cout = 0
                while(X1 < size && Y1 < size && cout<5){
                    model = model + checkbit(chess: board[Y1][X1],isblack:isblack)
                    X1 += 1
                    Y1 += 1
                    cout += 1
                }
                X2 = X-1
                Y2 = Y-1
                cout = 0
                while(X2 >= 0 && Y2 >= 0 && cout<5){
                    model = checkbit(chess: board[Y2][X2],isblack:isblack) + model
                    X2 -= 1
                    Y2 -= 1
                    cout += 1
                }
                break
            default:
                break
            }
//        print("\(direction) :  \(model)")
        let gradeP = grade(model: model)
//        print(gradeP)
        return gradeP
            }
    func checkbit(chess:Int,isblack:Bool)->String{
        if isblack{
        if chess == 2 {
            return "A"
        }else if chess == 0 {
            return "?"
        }else if chess == 1 {
            return "B"
            }
        }else{
            if chess == 1 {
                return "A"
            }else if chess == 0 {
                return "?"
            }else if chess == 2 {
                return "B"
            }
        }
        return ""
    }
    // mode 1 evaluation function
    func grade(model:String)->Int{
        if model.contains("AAAAA"){
            return 1000000
        }else if model.contains("?AAAA?"){
            return 90000
        }else if model.contains("AAAA?") || model.contains("?AAAA") || model.contains("A?AAA") || model.contains("AA?AA") || model.contains("AAA?A") {
            return 6000
        }else if model.contains("?AAA?") {
            return 5000
        }else if model.contains("?AA?A?") || model.contains("?A?AA?"){
            return 3000
        }else if model.contains("?AA?"){
            return 1000
        }else if model.contains("AA?")||(model.contains("?AA")){
            return 200
        }else if model.contains("?A?"){
            return 100
        }else if model.contains("A?") || model.contains("?A"){
            return 10
        }else{
            return 0
        }
    }
    
    
    // find the best 10 choice
    func basicAI(testmap:[[Int]],isblack:Bool)->[node]{
        var tenbest = [node]()
        var tenbestValue = [node]()
        var bestgrade = 0
        var bestpoint = [node]()
        var localmap = testmap
        for x in 0..<size{
            for y in 0..<size{
                if map[y][x] == 0 {
                    
                    var GradeForWhite = check4direction(board:testmap,X: x, Y: y,isblack: false,direction: "DtoU")
                        + check4direction(board:testmap,X: x, Y: y,isblack: false,direction: "LtoR")
                        + check4direction(board:testmap,X: x, Y: y,isblack: false, direction: "rightup")
                        + check4direction(board:testmap,X: x, Y: y,isblack: false,direction: "leftup")
                        + board[x][y]
                    
                    var GradeForBlack = check4direction(board:testmap,X: x, Y: y,isblack: true,direction: "DtoU")
                        + check4direction(board:testmap,X: x, Y: y,isblack: true,direction: "LtoR")
                        + check4direction(board:testmap,X: x, Y: y,isblack: true, direction: "rightup")
                        + check4direction(board:testmap,X: x, Y: y,isblack: true,direction: "leftup")
                        + board[x][y]
                    if isblack{
                        GradeForBlack = GradeForBlack + 100
                    }else{
                        GradeForWhite = GradeForWhite + 100
                    }
                    if GradeForBlack < GradeForWhite{
                        tenbest.append(node(x: x, y: y, value: GradeForWhite))
                    }else{
                        tenbest.append(node(x: x, y: y, value: GradeForBlack))
                    }
                }
            }
        }
        tenbest.sort { (node1, node2) -> Bool in
        abs(node1.value) > abs(node2.value)
        }
        for i in 0..<10{
         bestpoint.append(node(x: tenbest[i].x, y: tenbest[i].y, value: tenbest[i].value))
        }
        print()
        return bestpoint
    }

    func checkbit2(chess:Int,isblack:Bool)->String{
        if isblack{
        if chess == 2 {
            return "A"
        }else if chess == 0 {
            return "?"
        }else if chess == 1 {
            return "B"
        }
        }else{
            if chess == 2 {
                return "B"
            }else if chess == 0 {
                return "?"
            }else if chess == 1 {
                return "A"
            }
        }
        return ""
    }
    //mode 2  evaluation function
    func grade2(model:String,isblack:Bool)->Int{
        var B = 0
      
        //5
        if model.contains("AAAAA"){
            return 90000000
        }
        if model.contains("BBBBB"){
            return -90000000
        }
        
        //活4
        if model.contains("?AAAA?"){
            B = B + 4000000
        }
        if model.contains("?BBBB?"){
            B = B - 4000000
        }
        //死4
        //A
        if model.contains("BAAAA?"){
            B = B + 300000
        }
        if model.contains("ABBBB?") {
            B = B - 300000
        }
        if model.contains("?AAAAB"){
            B = B + 300000
        }
        if model.contains("?BBBBA"){
            B = B - 300000
        }
        //B
        if model.contains("BAAA?A"){
            B = B + 300000
        }
        if model.contains("ABBB?B") {
            B = B - 300000
        }
        if model.contains("A?AAAB"){
            B = B + 300000
        }
        if model.contains("B?BBBA"){
            B = B - 300000
        }
        //C
        if model.contains("BAA?AA"){
            B = B + 300000
        }
        if model.contains("ABB?BB") {
            B = B - 300000
        }
        if model.contains("AA?AAB"){
            B = B + 300000
        }
        if model.contains("BB?BBA"){
            B = B - 300000
        }
        //D
        if model.contains("BA?AAA"){
            B = B + 300000
        }
        if model.contains("AB?BBB") {
            B = B - 300000
        }
        if model.contains("AAA?AB"){
            B = B + 300000
        }
        if model.contains("BBB?BA"){
            B = B - 300000
        }
        //活3
        if model.contains("?AAA?"){
            B = B + 200000
        }
        if model.contains("?BBB?") {
            B = B - 200000
        }
        //死3
        //A
        if model.contains("?A?AA?"){
            B = B + 40000
        }
        if model.contains("?B?BB?") {
            B = B - 40000
        }
        if model.contains("?AA?A?"){
            B = B + 40000
        }
        if model.contains("?BB?B?") {
            B = B - 40000
        }
        //B
        if model.contains("BAAA??"){
            B = B + 1000
        }
        if model.contains("ABBB??") {
            B = B - 1000
        }
        if model.contains("??AAAB"){
            B = B + 1000
        }
        if model.contains("??BBBA") {
            B = B - 1000
        }
        //活2
       
        if model.contains("??AA??"){
            B = B + 2000
        }
        if model.contains("??BB??") {
            B = B - 2000
        }
        
        //死2
        if model.contains("BAA?"){
            B = B - 10
        }
        if model.contains("ABB?") {
            B = B + 10
        }
        if model.contains("?AAB"){
            B = B - 10
        }
        if model.contains("?BBA") {
            B = B + 10
        }
//        //活1
//        if model.contains("?A?"){
//            B = B + 100
//        }
//        if model.contains("?B?") {
//            B = B - 100
//        }
//        if model.contains("??A??"){
//            B = B + 120
//        }
//        if model.contains("??B??") {
//            B = B - 120
//        }
//        //死1
//        if model.contains("BA?"){
//            B = B - 20
//        }
//        if model.contains("AB?") {
//            B = B + 20
//        }
//        if model.contains("?AB"){
//            B = B - 20
//        }
//        if model.contains("?BA") {
//            B = B + 20
//        }
        
//        return isblack ? B : 0 - B
        return B
    }

    //scan board
    var c = 0
    func gradeBoard(board:[[Int]],isblack:Bool)->Int{
        c += 1
        print(c)
        var allgrade = 0
        var testmap = board
      
        for y in 0..<size{//横向
            var line1 = ""
            for x in 0..<size{
                line1 = line1 + checkbit2(chess: testmap[y][x], isblack: isblack)
            }
            allgrade = allgrade + grade2(model: line1,isblack:isblack )
            }
        for x in 0..<size{//纵向
            var line2 = ""
            for y in 0..<size{
                line2 = line2 + checkbit2(chess: testmap[y][x], isblack: isblack)
            }
            allgrade = allgrade + grade2(model: line2,isblack:isblack)
        }
        for i in 0..<size{//斜向右下对角上半
            var line3 = ""
            var x = i
            var y = 0
            while x<size && y < size {
                line3 = line3 + checkbit2(chess: testmap[y][x], isblack: isblack)
                x += 1
                y += 1
            }
             allgrade = allgrade + grade2(model: line3,isblack:isblack)
        }
        for i in 1..<size{//斜向右下对角下半
            var line3 = ""
            var x = 0
            var y = i
            while x<size && y < size {
                line3 = line3 + checkbit2(chess: testmap[y][x], isblack: isblack)
                x += 1
                y += 1
            }
            allgrade = allgrade+grade2(model: line3,isblack:isblack)

        }
        for i in 0..<size{//斜向左下对角上半
            var line4 = ""
            var x = i
            var y = 0
            while x >= 0 && y < size {
                line4 = line4 + checkbit2(chess: testmap[y][x], isblack: isblack)
                x -= 1
                y += 1
            }
             allgrade = allgrade+grade2(model: line4,isblack:isblack)
        }
        for i in 1..<size{//斜向左下对角上半
            var line4 = ""
            var x = size - 1
            var y = i
            while x >= 0 && y < size {
                line4 = line4 + checkbit2(chess: testmap[y][x], isblack: isblack)
                x -= 1
                y += 1
            }
            allgrade = allgrade+grade2(model: line4,isblack:isblack)
         }
        for i in testmap{
            print(i)
        }
        print("allgrade = \(allgrade) ")
        if isblack{
        return -1*allgrade
        }else{
            return allgrade
        }
    }
    
    func evaluate(testmap:[[Int]],isblack:Bool)->Int{
        var bestgrade = 0
        var localmap = testmap
        for x in 0..<size{
            for y in 0..<size{
                if map[y][x] == 0 {
                    
                    let GradeForWhite = check4direction(board:testmap,X: x, Y: y,isblack: false,direction: "DtoU")
                    + check4direction(board:testmap,X: x, Y: y,isblack: false,direction: "LtoR")
                    + check4direction(board:testmap,X: x, Y: y,isblack: false, direction: "rightup")
                    + check4direction(board:testmap,X: x, Y: y,isblack: false,direction: "leftup")
                    let GradeForBlack = check4direction(board:testmap,X: x, Y: y,isblack: true,direction: "DtoU")
                        + check4direction(board:testmap,X: x, Y: y,isblack: true,direction: "LtoR")
                        + check4direction(board:testmap,X: x, Y: y,isblack: true, direction: "rightup")
                        + check4direction(board:testmap,X: x, Y: y,isblack: true,direction: "leftup")
                   bestgrade = bestgrade + GradeForWhite - GradeForBlack
                }
            }
        }
       return bestgrade
    }

    
    var cout = 0
    var cutA = 0
    var cutB = 0
    //Minmax + Alpha-beta
    func Minmax(testmap:[[Int]],isblack:Bool,depth:Int,alpha:Int,beta:Int,bestnode:CGPoint)->node{
       var best = node(x: 0, y: 0, value: 0)
       var record = node(x: 0, y: 0, value: 0)
       var curnode:node = node(x: 0, y: 0, value: 0)
       var localtest = testmap
       var localalpha = alpha
       var localbeta = beta
       
//       let a = checkend(testmap: testmap, isblack: !isblack)
//       if a.isend{
//               return node(x: Int(a.p.x), y: Int(a.p.y), value: 9000000000)
//              }
        
        if depth == 0{
         
            cout += 1
            print("cout = \(cout)")
            return node(x: Int(bestnode.x), y: Int(bestnode.y), value: gradeBoard(board: testmap, isblack: isblack) + board[Int(bestnode.x)][Int(bestnode.y)] )
          
                    }else{
            var best10 = basicAI(testmap: localtest, isblack: isblack)
           
            if isblack{//max
                var V = node(x: 0, y: 0, value: -9000000000)
                
                for p in best10 {
                         let X = Int(p.x)
                         let Y = Int(p.y)
                        //limit the area
                       // if abs(X - Int(bestnode.x)) < 4 && abs(Y - Int(bestnode.y)) < 4{
                        if localtest[Y][X] == 0 {
                            
                            localtest[Y][X] = 1
                            V = Minmax(testmap:localtest, isblack: !isblack, depth: depth - 1, alpha: localalpha,beta:localbeta,bestnode: CGPoint(x:X,y:Y))
                            localtest[Y][X] = 0
                            if localalpha < V.value{
                                localalpha = V.value
                                record = node(x: X, y: Y, value: localalpha)
                            }
                            if localalpha >= localbeta{
                                cutA += 1
                                print("cutA = \(cutA)")
                                return node(x: X, y: Y, value: localbeta)
                            }
                           }
                         //}
                     }
                  
               best = record
            }else{//min
                var V = node(x: 0, y: 0, value: 9000000000)
                
                for p in best10 {
                    let X = Int(p.x)
                    let Y = Int(p.y)
                    
                        //limit the area
                        //if abs(X - Int(bestnode.x)) < 4 && abs(Y - Int(bestnode.y)) < 4 {
                        if localtest[Y][X] == 0 {
                            localtest[Y][X] = 2
                            V = Minmax(testmap:localtest, isblack: !isblack, depth: depth - 1, alpha: localalpha,beta:localbeta,bestnode:CGPoint(x: X, y: Y))
                            localtest[Y][X] = 0
                            if localbeta > V.value{
                                localbeta = V.value
                                record = node(x: X, y: Y, value: localbeta)
                            }
                            if localalpha>=localbeta{
                                cutB += 1
                                print("cutB = \(cutB)")
                                return node(x: X, y: Y, value: localalpha)
                            }
                         }
                      //}
                   }
                }
             
            best = record
        }
        print("best x=\(best.x) y=\(best.y) value=\(best.value)")
        return best
    }
    
    
    func checkend(testmap:[[Int]],isblack:Bool)->(isend:Bool,p:CGPoint){
    var tenbest = [node]()
    var tenbestValue = [Int]()
    var bestgrade = 0
    var bestpoint = [CGPoint]()
    var localmap = testmap
    for x in 0..<size{
    for y in 0..<size{
    if map[y][x] == 0 {
    
        if check4direction(board:testmap,X: x, Y: y,isblack: isblack,direction: "DtoU") >= 1000000 {
            return (true,CGPoint(x: x, y: y))
        }
        if check4direction(board:testmap,X: x, Y: y,isblack: isblack,direction: "LtoR") >= 1000000 {
            return (true,CGPoint(x: x, y: y))
        }
        if check4direction(board:testmap,X: x, Y: y,isblack: isblack,direction: "rightup") >= 1000000 {
            return (true,CGPoint(x: x, y: y))
        }
        if check4direction(board:testmap,X: x, Y: y,isblack: isblack,direction: "leftup") >= 1000000 {
            return (true,CGPoint(x: x, y: y))
        }
    
    }
    }
    }
   
    print()
    return (false,CGPoint(x: 0, y: 0))
    }
    
}
