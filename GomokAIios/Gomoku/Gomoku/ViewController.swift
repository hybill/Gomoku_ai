//
//  ViewController.swift
//  Gomoku
//
//  Created by YWQ on 10/22/16.
//  Copyright © 2016 ywq. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    var boardView:CheckerboardView?
    var changeMode = UIButton()
    var reStartBtn = UIButton()
    var changeBoardButton = UIButton()
    var AI = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setup(){
        self.view.backgroundColor = UIColor(white: 1, alpha: 0.8)
//        map.removeAll()
        for i in map {
            print(i)
            print()
        }
        //add gane table
        let boardView = CheckerboardView()
        self.view.addSubview(boardView)
        boardView.snp.makeConstraints{
            (make)->Void in
            make.top.equalTo(120)
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.height.equalTo(boardView.snp.width)
        }
        self.boardView = boardView
        
        //chageboard
        let changeBoardButton = UIButton(type: UIButtonType.custom)
        changeBoardButton.setTitle("Gomoku", for: UIControlState.normal)
        changeBoardButton.setTitleColor(UIColor.gray, for: UIControlState.disabled)
        changeBoardButton.backgroundColor = UIColor(colorLiteralRed:200/255.0,green:160/255.0,blue:130/255.0,alpha:1)
        changeBoardButton.frame = CGRect(x: boardView.frame.midX - boardView.frame.width*0.3, y: boardView.frame.midY - 50, width: boardView.frame.width , height: 35)
        changeBoardButton.layer.cornerRadius = 4
        self.view.addSubview(changeBoardButton)
        changeBoardButton.snp.makeConstraints{
            (make)->Void in
            make.centerX.equalTo((changeBoardButton.superview?.snp.centerX)!)
            make.width.equalTo(80)
            make.height.equalTo(35)
            make.bottom.equalTo(boardView.snp.top).offset(-20)
        }
//        self.changeBoardButton = changeBoardButton
//        changeBoardButton.addTarget(self, action:#selector(changeBoard), for: UIControlEvents.touchUpInside)
   
        //new game
        let reStartBtn = UIButton(type: UIButtonType.custom)
        reStartBtn.setTitle("restart", for: UIControlState.normal)
        reStartBtn.setTitleColor(UIColor.gray, for: UIControlState.disabled)
        reStartBtn.backgroundColor = UIColor(colorLiteralRed: 200/255.0, green: 160/255.0, blue: 130/255.0, alpha: 1)
        reStartBtn.frame = CGRect(x: boardView.frame.minX, y: boardView.frame.maxY+15, width: boardView.frame.width*0.45, height: 30)
        reStartBtn.layer.cornerRadius = 4
        self.view.addSubview(reStartBtn)
        reStartBtn.snp.makeConstraints{
            make in
            make.top.equalTo(boardView.snp.bottom).offset(20)
            make.height.equalTo(35)
            make.right.equalTo(-30)
            make.width.equalTo(100)
        }
        self.reStartBtn = reStartBtn
        reStartBtn.addTarget(self, action: #selector(newGame), for: UIControlEvents.touchUpInside)
        
        let changeMode = UIButton(type: UIButtonType.custom)
        changeMode.setTitle("mode 1", for: UIControlState.normal)
        changeMode.setTitleColor(UIColor.gray, for: UIControlState.disabled)
        changeMode.backgroundColor = UIColor(colorLiteralRed: 200/255.0, green: 160/255.0, blue: 130/255.0, alpha: 1)
        changeMode.frame = CGRect(x: boardView.frame.minX, y: boardView.frame.maxY+15, width: boardView.frame.width*0.45, height: 30)
        changeMode.layer.cornerRadius = 4
        self.view.addSubview(changeMode)
        changeMode.snp.makeConstraints{
            make in
            make.top.equalTo(boardView.snp.bottom).offset(20)
            make.height.equalTo(35)
            make.left.equalTo(30)
            make.width.equalTo(100)
        }
        self.changeMode = changeMode
        changeMode.addTarget(self, action: #selector(newMode), for: UIControlEvents.touchUpInside)
        
        let AI = UIButton(type: UIButtonType.custom)
        AI.setTitle("AI put chess", for: UIControlState.normal)
        AI.setTitleColor(UIColor.gray, for: UIControlState.disabled)
        AI.backgroundColor = UIColor(colorLiteralRed: 200/255.0, green: 160/255.0, blue: 130/255.0, alpha: 1)
        AI.frame = CGRect(x: boardView.frame.minX, y: boardView.frame.maxY+15, width: boardView.frame.width*0.45, height: 30)
        AI.layer.cornerRadius = 4
        self.view.addSubview(AI)
        AI.snp.makeConstraints{
            make in
            make.top.equalTo(boardView.snp.bottom).offset(20)
            make.height.equalTo(35)
            make.centerX.equalTo(boardView.snp.centerX)
            make.width.equalTo(100)
        }
        self.AI = AI
        AI.addTarget(self, action: #selector(findbest), for: UIControlEvents.touchUpInside)


    }
    
    func checkResult(X:Int,Y:Int)->Bool {
        let a = Agent()
        if (a.check4direction(board:map,X: X, Y: Y,isblack:isBlack, direction: "DtoU") >= 1000000 ||
        a.check4direction(board:map,X: X, Y: Y, isblack:isBlack,direction: "LtoR") >= 1000000 ||
        a.check4direction(board:map,X: X, Y: Y, isblack:isBlack,direction: "leftup") >= 1000000 ||
        a.check4direction(board:map,X: X, Y: Y, isblack:isBlack,direction: "rightup") >= 1000000 ){
            let alertView = UIAlertView(title: "gameover", message: isBlack ? "black win!" : "white win", delegate: self, cancelButtonTitle: "ok")
            alertView.show()
            return true
        }
        return false
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func changeBoard(btn:UIButton){
//        self.boardView?.changeBoardLevel()
//        changeBoardButton.setTitle(btn.currentTitle=="Easy" ? "Hard" : "Easy", for: UIControlState.normal)
//    }
//    func backOneStep(sender:UIButton){
////        if(self.)
//    }
    func newGame() {
        self.boardView?.newGame()
    }
    func newMode() {
        if mode == 1 {
            mode = 2
        }else{
            mode = 1
        }
        if mode == 2{
        let input = UIAlertController(title: "choose search depth", message: "please less than 4(defualut=\(Gdepth)", preferredStyle: UIAlertControllerStyle.alert)
            input.addTextField{ (textField: UITextField!) -> Void in
                textField.placeholder = "\(Gdepth)"
            }
            let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {aciton in
                if let a = Int((input.textFields?.first?.text!)!){
                    Gdepth = a
                }
                })
            input.addAction(ok)
            show(input, sender:nil)
         
            }
        changeMode.setTitle("mode \(mode)", for: UIControlState.normal)
    }
    func findbest(){
        let a = Agent()
        let x = boardView
        if mode == 1{
            DispatchQueue.main.async {
                let p = a.basicAI(testmap: map, isblack: isBlack)
                x?.drawchess(X: Int(p[0].x), Y: Int(p[0].y))
            }
        }else{
            //                a.gradeBoard(board: map, isblack: !isBlack)
            if isBlack{
            DispatchQueue.main.async {
                let p = a.Minmax(testmap: map, isblack: true, depth: Gdepth, alpha: -9000000000, beta: 9000000000,bestnode: CGPoint(x: 0, y: 0))//white
                x?.drawchess(X: Int(p.x), Y: Int(p.y))
            }
            }else{
                DispatchQueue.main.async {
                    let p = a.Minmax(testmap: map, isblack: true, depth: Gdepth, alpha: -9000000000, beta: 9000000000,bestnode: CGPoint(x: 0, y: 0))//white
                    x?.drawchess(X: Int(p.x), Y: Int(p.y))
                }
            }
        }
    }
}


