//
//  Globle.swift
//  Gomoku
//
//  Created by YWQ on 11/22/16.
//  Copyright © 2016 ywq. All rights reserved.
//
import UIKit
import Foundation

let kBoardSpace:CGFloat = 20
let kGridCount:CGFloat = CGFloat(size)
let kChessmanSizeRatio:CGFloat = 0.8
var isBlack = true
var mode = 1
let ScreenW = UIScreen.main.bounds.size.width
var size = 15//15line
var map:[[Int]] = [[Int]](repeating:[Int](repeating:0,count:size+1),count:size+1)
var Gdepth = 2
var Gend = false
var AIfight = false
var board:[[Int]] =  [[Int]](repeating:[Int](repeating:0,count:size),count:size)
//    [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
//            [0,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
//            [0,1,2,2,2,2,2,2,2,2,2,2,2,1,0],
//            [0,1,2,3,3,3,3,3,3,3,3,3,2,1,0],
//            [0,1,2,3,4,4,4,4,4,4,4,3,2,1,0],
//            [0,1,2,3,4,5,5,5,5,5,4,3,2,1,0],
//            [0,1,2,3,4,5,6,6,6,5,4,3,2,1,0],
//            [0,1,2,3,4,5,6,7,6,5,4,3,2,1,0],
//            [0,1,2,3,4,5,6,6,6,5,4,3,2,1,0],
//            [0,1,2,3,4,5,5,5,5,5,4,3,2,1,0],
//            [0,1,2,3,4,4,4,4,4,4,4,3,2,1,0],
//            [0,1,2,3,3,3,3,3,3,3,3,3,2,1,0],
//            [0,1,2,2,2,2,2,2,2,2,2,2,2,1,0],
//            [0,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
//            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]
