//
//  CheckerboardView.swift
//  Gomoku
//
//  Created by YWQ on 10/24/16.
//  Copyright © 2016 ywq. All rights reserved.
//

import Foundation
import UIKit

class CheckerboardView:UIView{
    
    var gridWidth:CGFloat = 0
    var sameChessmanArray = NSMutableArray()
    var chessmanDict = NSMutableDictionary()
    var lastKey:String? = nil
    var isHighLevel = false
    var gridCount:CGFloat = CGFloat(size)
    var isOver = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
         self.frame = CGRect(x: 20, y: 30, width: UIScreen.main.bounds.width - 20, height: UIScreen.main.bounds.width - 20)   
        self.setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  
    func setUp()  {
        self.backgroundColor = UIColor(colorLiteralRed: 200/255.0, green: 160/255.0, blue: 130/255.0, alpha: 1)
        self.drawBackground(size: self.frame.size)
//        let tap1 = UIGestureRecognizer(target: self, action: #selector(self.tapBoard(_:)))
//        self.addGestureRecognizer(tap1)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBoard(_:)))
        self.addGestureRecognizer(tapGesture)
    }
    //put chess
    func tapBoard(_ sender:UITapGestureRecognizer){
        if !Gend {
        let point = sender.location(in: sender.view)
        let X:Int = Int((point.x - kBoardSpace + 0.5 * self.gridWidth)/self.gridWidth)
        let Y:Int = Int((point.y - kBoardSpace + 0.5 * self.gridWidth)/self.gridWidth)
        let key = "\(X)-\(Y)"
       
        print(key)
        for i in map{
            print(i)
        }
        print()
        if map[Y][X] != 0
        {
            print("space beeb taken")
        }else{
        if isBlack{
        drawchess(X: X, Y: Y)
        let a = Agent()
            if mode == 1{
                DispatchQueue.main.async {
                    let p = a.basicAI(testmap: map, isblack: isBlack)
                    self.drawchess(X: Int(p[0].x), Y: Int(p[0].y))
                }
            }else{
//                a.gradeBoard(board: map, isblack: !isBlack)
                DispatchQueue.main.async {
                let p = a.Minmax(testmap: map, isblack: true, depth: Gdepth, alpha: -9000000000, beta: 9000000000,bestnode: CGPoint(x: X, y: Y))//white
                self.drawchess(X: Int(p.x), Y: Int(p.y))
                }
             }
           }
        }
    }
}

    func drawchess(X:Int,Y:Int){
        if isBlack {
            map[Y][X] = 2
        }else{
            map[Y][X] = 1
        }
        let chessman = self.chessman()
        chessman.center = CGPoint(x: kBoardSpace + CGFloat(X) * self.gridWidth, y: kBoardSpace+CGFloat(Y)*self.gridWidth)
        self.addSubview(chessman)
        let vc = ViewController()
        if vc.checkResult(X: X, Y: Y){
           print("end")
        }
        isBlack = !isBlack
        
    }
    
    
    func drawBackground(size:CGSize){
        print("drawgrid")
        self.gridWidth = (size.width - 2*kBoardSpace)/(self.gridCount-1)
         UIGraphicsBeginImageContext(size)
        if let context:CGContext = UIGraphicsGetCurrentContext()
        // draw lines to form a 10x10 cell grid
       {
        context.setFillColor(UIColor.black.cgColor)
        context.setLineWidth(1)
        for i in 0..<Int(self.gridCount) {
            // draw horizontal grid line
            let iF = CGFloat(i)
            context.move(to: CGPoint(x:kBoardSpace + iF * self.gridWidth , y:kBoardSpace))
            context.addLine(to: CGPoint(x:kBoardSpace + iF * self.gridWidth ,y:kBoardSpace + CGFloat(self.gridCount-1) * self.gridWidth))
         
        }
        for i in 0..<Int(self.gridCount) {
            // draw vertical grid line
            let iF = CGFloat(i)
            context.move(to: CGPoint(x:kBoardSpace, y:kBoardSpace  + iF * self.gridWidth))
            context.addLine(to: CGPoint(x:kBoardSpace + CGFloat(self.gridCount-1) * self.gridWidth , y:kBoardSpace + iF * self.gridWidth))}
        context.strokePath()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        let uiimageView = UIImageView(image: image)
        self.addSubview(uiimageView)
        }
         UIGraphicsEndImageContext()
    }
    
    func changeBoardLevel(){
        for view in self.subviews{
            view.removeFromSuperview()
        }
        self.newGame()
        self.isHighLevel = !self.isHighLevel
        self.drawBackground(size: self.bounds.size)
        
    }
    func newGame(){
        Gend = false
        self.gridCount = self.isHighLevel ? kGridCount : kGridCount-4
        self.isOver = false
        self.lastKey = nil
        self.sameChessmanArray.removeAllObjects()
        self.isUserInteractionEnabled = true
        self.chessmanDict.removeAllObjects()
        for view in self.subviews{
            if view.isKind(of: UIImageView.self){
                continue
            }
            view.removeFromSuperview()
        }
        map = [[Int]](repeating:[Int](repeating:0,count:size),count:size)
        isBlack = true
    }
    

    func chessman()->UIView{
        let chessmanView = UIView(frame: CGRect(x: 0, y: 0, width: self.gridWidth*kChessmanSizeRatio, height: self.gridWidth*kChessmanSizeRatio))
        chessmanView.layer.cornerRadius = chessmanView.frame.width * 0.5
        chessmanView.backgroundColor = isBlack ? UIColor.black : UIColor.white
        return chessmanView
    }
    
  
    
}
